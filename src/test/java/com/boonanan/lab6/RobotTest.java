package com.boonanan.lab6;

import static org.junit.Assert.assertEquals;


import org.junit.Test;

public class RobotTest {
    @Test
    public void Downbound() {
        Robot Marine = new Robot("Marine", 'N', 0, 0);
        assertEquals(false, Marine.Down());
    }
    @Test
    public void Upbound() {
        Robot Marine = new Robot("Marine", 'N', 0, 19);
        assertEquals(false, Marine.Up());
    }
    @Test
    public void Leftbound() {
        Robot Marine = new Robot("Marine", 'N', 0, 0);
        assertEquals(false, Marine.Left());
    }
    @Test
    public void Rightbound() {
        Robot Marine = new Robot("Marine", 'N', 19, 0);
        assertEquals(false, Marine.Right());
    }
    @Test
    public void Down() {
        Robot Marine = new Robot("Marine", 'N', 0, 5);
        assertEquals(true, Marine.Down());
    }
    @Test
    public void Up() {
        Robot Marine = new Robot("Marine", 'N', 0, 5);
        assertEquals(true, Marine.Up());
    }
    @Test
    public void Left() {
        Robot Marine = new Robot("Marine", 'N', 5, 0);
        assertEquals(true, Marine.Left());
    }
    @Test
    public void Right() {
        Robot Marine = new Robot("Marine", 'N', 5, 0);
        assertEquals(true, Marine.Right());
    }
    @Test
    public void NoInitialXY() {
        Robot Marine = new Robot("Marine", 'N');
        assertEquals(0, Marine.Getx());
        assertEquals(0, Marine.Gety());
    }
    @Test
    public void MutiMove() {
        Robot Marine = new Robot("Marine", 'N', 5, 0);
        Marine.Down(2);
        assertEquals(0, Marine.Gety());
        Marine.Right(5);
        assertEquals(10, Marine.Getx());
        Marine.Up(5);
        assertEquals(5, Marine.Gety());
        Marine.Left(5);
        assertEquals(5, Marine.Getx());
    }

}
