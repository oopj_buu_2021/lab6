package com.boonanan.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void WithdrawBelowZero() {
        BookBank ded = new BookBank("ded", 100.0);
        ded.withdraw(150);
        assertEquals(100.0, ded.Getbalance(), 0.001);
    }
    @Test
    public void NormalDeposit() {
        BookBank ded = new BookBank("ded", 0.0);
        ded.deposit(150);
        assertEquals(150.0, ded.Getbalance(), 0.001);
    }
    @Test
    public void NormalWithdraw() {
        BookBank ded = new BookBank("ded", 100.0);
        ded.withdraw(50);
        assertEquals(50.0, ded.Getbalance(), 0.001);
    }
    @Test
    public void NegativelDeposit() {
        BookBank ded = new BookBank("ded", 100.0);
        ded.withdraw(-50);
        assertEquals(100.0, ded.Getbalance(), 0.001);
    }
    @Test
    public void NegativeWithdraw() {
        BookBank ded = new BookBank("ded", 100.0);
        ded.withdraw(-50);
        assertEquals(100.0, ded.Getbalance(), 0.001);
    }

}
