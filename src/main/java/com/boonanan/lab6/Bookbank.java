package com.boonanan.lab6;

public class BookBank {
    private String name;
    private double balance;

    public BookBank (String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    boolean deposit(int money) {
        if (money<0) return false;
        balance = balance + money;
        print();
        return true;
    }

    boolean withdraw(int money) {
        if (money<0) return false;
        if (balance - money < 0) return false;
        balance = balance - money;
        print();
        return true;
    }

    void print() {
        System.out.println(name + " " + balance);
    }
    public String Getname() {
        return name;
    }
    public double Getbalance() {
        return balance;
    }
    
}
