package com.boonanan.lab6;

public class RobotApp {
    public static void main(String[] args) {
        Robot Nakiri = new Robot("Nakiri", 'N');
        Robot IRyS = new Robot("IRyS", 'I', 19, 19);
        Nakiri.Location();
        IRyS.Location();
        Nakiri.Up();
        IRyS.Up(3);
        Nakiri.Left();

        for (int i = Robot.MinY; i <= Robot.MaxY; i++) {
            for (int o = Robot.MinX; o <= Robot.MaxX; o++) {
                if (Nakiri.Getx() == o && Nakiri.Gety() == i) {
                    System.out.print(Nakiri.Getsymbol());
                }else if (IRyS.Getx() == o && IRyS.Gety() == i){
                    System.out.print(IRyS.Getsymbol());
                } else {
                    System.out.print('-');
                }
            }
            System.out.println();
        }

    } 
    
}
