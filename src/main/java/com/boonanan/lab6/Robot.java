package com.boonanan.lab6;

public class Robot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public static final int MinX = 0;
    public static final int MinY = 0;
    public static final int MaxX = 19;
    public static final int MaxY = 19;
    //ของผมมันเป็น (0,0) กับ (19,19) นะครับ ตามแกน(x,y)ปกติ

    public Robot(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }
    public Robot(String name, char symbol) {
        this(name, symbol, 0, 0);
    }

    boolean Up() {
        if(y==MaxY) return LocationFalse();
        y++;
        Location();
        return true;
    }
    boolean Down() {
        if(y==MinY) return LocationFalse();
        y--;
        Location();
        return true;
    }
    boolean Left() {
        if(x==MinX) return LocationFalse();
        x--;
        Location();
        return true;
    }
    boolean Right() {
        if(x==MaxX) return LocationFalse();
        x++;
        Location();
        return true;
    }

    public void Location() {
        System.out.println(name + " x:" + x + " y:" + y);
    }
    public boolean LocationFalse() {
        Location();
        return false;
    }
    public int Getx() {
        return x;
    }
    public int Gety() {
        return y;
    }
    public String Getname() {
        return name;
    }
    public char Getsymbol() {
        return symbol;
    }
    public boolean Up(int step) {
        for (int i = 0; i < step; i++) {
            if(!Up()) {
                return false;
            }
        }
        return true;
    }
    public boolean Down(int step) {
        for (int i = 0; i < step; i++) {
            if(!Down()) {
                return false;
            }
        }
        return true;
    }
    public boolean Left(int step) {
        for (int i = 0; i < step; i++) {
            if(!Left()) {
                return false;
            }
        }
        return true;
    }
    public boolean Right(int step) {
        for (int i = 0; i < step; i++) {
            if(!Right()) {
                return false;
            }
        }
        return true;
    }


}
